VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Coefficient"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "������������ �����������, ����������� � ���������, ��������� ��� ������ �����."
'@ModuleDescription "������������ �����������, ����������� � ���������, ��������� ��� ������ �����."
'@PredeclaredId
'@Folder("Entities")
Option Explicit

Implements IMathematics

Private Type TState
    CurrentValue As IMathematics
End Type

Private This As TState

'@Description "������� ��������� ������ Coefficient."
Public Function Create(ByVal Value As Double) As Coefficient
Attribute Create.VB_Description = "������� ��������� ������ Coefficient."
    Dim Result As Coefficient
    Set Result = New Coefficient
    
    Set Result.CurrentValue = AbcMathematics.Create(IIf(Value = 0#, vbNullString, Value))
    Set Create = Result
End Function

Public Property Get CurrentValue() As IMathematics
    Set CurrentValue = This.CurrentValue
End Property
Friend Property Set CurrentValue(ByVal Value As IMathematics)
    Set This.CurrentValue = Value
End Property

Private Sub IMathematics_Add(ByVal Value As Double)
    This.CurrentValue.Add Value
End Sub

Private Sub IMathematics_Substract(ByVal Value As Double)
    This.CurrentValue.Substract Value
End Sub

Private Sub IMathematics_Multiply(ByVal Value As Double)
    This.CurrentValue.Multiply Value
End Sub

Private Sub IMathematics_Divide(ByVal Value As Double)
    This.CurrentValue.Divide Value
End Sub

Private Property Get IMathematics_Value() As IAbcElement
    GuardClauses.GuardDefaultInstance Me, Coefficient
    Set IMathematics_Value = This.CurrentValue.Value
End Property
