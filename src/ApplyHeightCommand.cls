VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ApplyHeightCommand"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "��������� �������� �� ������ ��������. "
'@PredeclaredId
'@ModuleDescription "��������� �������� �� ������ ��������. "
'@Folder("Entities.Commands")
Option Explicit

Implements ICommand

Private Type TState
    Reciever As IUnitRate
    Height As Double
    Clarification As String
    Coefficient As Double
End Type

Private This As TState

Public Function Create(ByVal Reciever As IUnitRate, ByVal Height As Double, ByVal Text As String) As ICommand
    Dim Command As ApplyHeightCommand
    Set Command = New ApplyHeightCommand
    
    Set Command.Reciever = Reciever
    Command.Height = Height
    Command.Clarification = Text
    Command.Coefficient = 1 + 0.04 * (Height - 5)
    
    Set Create = Command
End Function

Public Property Get Reciever() As IUnitRate
    Set Reciever = This.Reciever
End Property
Friend Property Set Reciever(ByVal Value As IUnitRate)
    GuardClauses.GuardNullReference Value
    Set This.Reciever = Value
End Property

Public Property Get Height() As Double
    Height = This.Height
End Property
Friend Property Let Height(ByVal Value As Double)
    This.Height = Value
End Property

Public Property Get Clarification() As String
    Clarification = This.Clarification
End Property
Friend Property Let Clarification(ByVal Value As String)
    This.Clarification = Value
End Property

Public Property Get Coefficient() As Double
    Coefficient = This.Coefficient
End Property
Friend Property Let Coefficient(ByVal Value As Double)
    This.Coefficient = Value
End Property

Private Sub ICommand_Execute(Optional ByVal Reciever As IUnitRate)
    If This.Height <= 5 Then Exit Sub
    
    Dim CoefficientAmendmend As IAmendmend
    Set CoefficientAmendmend = Amendmend.Create("�", 1, This.Coefficient)
    
    If This.Reciever.Amendmends.Exists(CoefficientAmendmend) Then
        Set CoefficientAmendmend = This.Reciever.Amendmends.Item(CoefficientAmendmend)
        CoefficientAmendmend.Coefficient.Multiply This.Coefficient
    Else
        This.Reciever.Amendmends.Add CoefficientAmendmend
    End If
    This.Reciever.Coefficients.Add This.Coefficient
    This.Reciever.Append This.Clarification
End Sub
