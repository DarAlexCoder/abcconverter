VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AbcUnit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'@Folder("Entities.Abc")
'@PredeclaredId
Option Explicit

Implements IUnit
Implements IAbcElement

Private Type TState
    Multiplier As Long
    Text As String
End Type

Private This As TState

Public Function From(ByVal Text As String) As IUnit
    Dim TrimedText As String
    TrimedText = Trim$(Text)
    
    Dim MultiplierText As String
    MultiplierText = RegEx.Match(TrimedText, "^\d*")
    
    Dim Result As AbcUnit
    Set Result = New AbcUnit
    
    With Result
        If 0 < Len(MultiplierText) Then
            .Multiplier = CInt(MultiplierText)
        End If
        .Text = Trim$(Replace(TrimedText, MultiplierText, vbNullString))
    End With
    
    Set From = Result
End Function

Private Sub Class_Initialize()
    This.Multiplier = 1
End Sub

Private Property Get IUnit_Text() As String
    IUnit_Text = This.Text
End Property
Friend Property Let Text(ByVal Value As String)
    This.Text = Value
End Property

Private Property Get IUnit_Multiplier() As Long
    IUnit_Multiplier = This.Multiplier
End Property
Friend Property Let Multiplier(ByVal Value As Long)
    This.Multiplier = Value
End Property

Private Property Get IUnit_Convert() As IAbcElement
    Set IUnit_Convert = Me
End Property

Private Function IAbcElement_ToAbc() As String
    IAbcElement_ToAbc = IIf(This.Multiplier = 1, vbNullString, "." & This.Multiplier)
End Function
