Attribute VB_Name = "Header_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

#Const LateBind = LateBindTests

#If LateBind Then
    Private Assert As Object
#Else
    Private Assert As Rubberduck.AssertClass
#End If

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    #If LateBind Then
        Set Assert = CreateObject("Rubberduck.AssertClass")
    #Else
        Set Assert = New Rubberduck.AssertClass
    #End If
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub Prefix_CreateCaption_�()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateCaption("test")

    'Assert:
    Assert.AreEqual "�", Sut.Prefix

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Number_CreateCaption_ReturnsNumberTwo()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateCaption("test")

    'Assert:
    Assert.AreEqual "2", Sut.Number

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Description_CreateCaption_IsNotNothing()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateCaption("test")

    'Assert:
    Assert.IsNotNothing Sut.Description

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Description_CreateCaption_EqualsConstructorText()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateCaption("test")

    'Assert:
    Assert.AreEqual "test", Sut.Description.Content

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub ToAbc_CreateCaption_EqualTo()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateCaption("test")

    'Assert:
    Assert.AreEqual "�2test*", Sut.ToAbc

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Prefix_CreateSection_�()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateSection("test")

    'Assert:
    Assert.AreEqual "�", Sut.Prefix

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Number_CreateSection_ReturnsNumberTwo()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateSection("test")

    'Assert:
    Assert.AreEqual "2", Sut.Number

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Description_CreateSection_IsNotNothing()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateSection("test")

    'Assert:
    Assert.IsNotNothing Sut.Description

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Description_CreateSection_EqualsConstructorText()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateSection("test")

    'Assert:
    Assert.AreEqual "test", Sut.Description.Content

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub ToAbc_CreateSection_EqualTo()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Header

    'Act:
    Set Sut = Header.CreateSection("test")

    'Assert:
    Assert.AreEqual "�2test*", Sut.ToAbc

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
