VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Summator"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'@Folder "Entities"

Option Explicit

Private Rates_ As Dictionary

Public Sub Add(ByVal Rate As IUnitRate)
    If Rate Is Nothing Then Exit Sub
    
    With Rates_
        If .Exists(Rate.Code) Then
            .Item(Rate.Code).Add Rate.Quantity
        Else
            .Add Key:=Rate.Code, Item:=Rate
        End If
    End With
End Sub

Public Property Get IsEmpty() As Boolean
    IsEmpty = Rates_.Count = 0
End Property

Public Property Get Count() As Long
    Count = Rates_.Count
End Property

Private Sub Class_Initialize()
    Set Rates_ = New Dictionary
End Sub

Private Sub Class_Terminate()
    Set Rates_ = Nothing
End Sub

Public Function ToArray() As Variant
    If IsEmpty Then Exit Function
    
    Dim Result() As Variant
    ReDim Result(1 To Rates_.Count + 1, 1 To 1)
    
    Result(1, 1) = "���������� ������*"
    
    Dim Rate As Variant
    Dim Index As Long: Index = 2
    For Each Rate In Rates_.Items
        Result(Index, 1) = Rate.ToString()
        Index = Index + 1
    Next
    
    ToArray = Result
End Function
