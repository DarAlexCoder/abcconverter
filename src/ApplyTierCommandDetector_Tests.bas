Attribute VB_Name = "ApplyTierCommandDetector_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

Private Assert As Object
Private Sut_ As ICommandDetector

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    Set Assert = CreateObject("Rubberduck.AssertClass")
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestInitialize
Private Sub TestInitialize()
    'This method runs before every test in the module..
    Set Sut_ = New ApplyTierCommandDetector
End Sub

'@TestCleanup
Private Sub TestCleanup()
    'this method runs after every test in the module.
    Set Sut_ = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub Detect_DottedHeight_IsTrue()
    Assert.IsTrue Detect_Helper("h=8.02 �=2")
    Assert.IsNotNothing Sut_.Command
End Sub

Private Function Detect_Helper(ByVal Name As String) As Boolean
    On Error GoTo TestFail
    
    Dim Actual As Boolean
    Dim CorrectEntry As IEntry

    Set CorrectEntry = Entry.Create("code", Name, "unit", 10#)
    Actual = Sut_.TryDetect(CorrectEntry, UnitRate)

    Detect_Helper = Actual

TestExit:
    Set CorrectEntry = Nothing
    Exit Function
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Function

'@TestMethod("Uncategorized")
Private Sub Detect_CommaHeight_IsTrue()
    Assert.IsTrue Detect_Helper("������ ������������� h=8,02 �=2")
    Assert.IsNotNothing Sut_.Command
End Sub

'@TestMethod("Uncategorized")
Private Sub Detect_WrongText_IsFalse()
    Assert.IsFalse Detect_Helper("�=6,0")
    Assert.IsNothing Sut_.Command
End Sub
