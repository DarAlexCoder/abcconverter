VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Header"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'@PredeclaredId
'@Folder("Entities")
Option Explicit

Implements IAbcElement

Private Prefix_ As String
Private Number_ As String
Private Description_ As ITextElement

'@Description "�������� ������� (�������������) ���������."
Public Property Get Prefix() As String
Attribute Prefix.VB_Description = "�������� ������� (�������������) ���������."
    Prefix = Prefix_
End Property
Friend Property Let Prefix(ByVal Value As String)
    Prefix_ = Value
End Property

'@Description "�������� ����� (���) ���������. "
Public Property Get Number() As String
Attribute Number.VB_Description = "�������� ����� (���) ���������. "
    Number = Number_
End Property
Friend Property Let Number(ByVal Value As String)
    Number_ = Value
End Property

'@Description "�������� ������� (�������������) ���������."
Public Property Get Description() As ITextElement
Attribute Description.VB_Description = "�������� ������� (�������������) ���������."
    Set Description = Description_
End Property
Friend Property Set Description(ByVal Value As ITextElement)
    Set Description_ = Value
End Property

'@Description "������� ��������� ��������� � �������� �������."
Public Function CreateCaption(ByVal Text As String) As Header
Attribute CreateCaption.VB_Description = "������� ��������� ��������� � �������� �������."
    GuardClauses.GuardNonDefaultInstance Me, Header
    Set CreateCaption = CreateHeader(Prefix:="�", Number:="2", Text:=Text)
End Function

Private Function CreateHeader(ByVal Prefix As String, ByVal Number As String, ByVal Text As String) As Header
    Dim Caption As Header
    Set Caption = New Header
    Caption.Prefix = Prefix
    Caption.Number = Number
    Set Caption.Description = TextElement.Create(Text)
    
    Set CreateHeader = Caption
End Function

'@Description "������� ��������� ������� � �������� �������."
Public Function CreateSection(ByVal Text As String) As Header
Attribute CreateSection.VB_Description = "������� ��������� ������� � �������� �������."
    GuardClauses.GuardNonDefaultInstance Me, Header
    Set CreateSection = CreateHeader(Prefix:="�", Number:="2", Text:=Text)
End Function

'@Description "����������� ��������� � ������, ��������������� �������� ����� �� ���."
Private Function IAbcElement_ToAbc() As String
Attribute IAbcElement_ToAbc.VB_Description = "����������� ��������� � ������, ��������������� �������� ����� �� ���."
    IAbcElement_ToAbc = Prefix & Number & Description.Content & "*"
End Function

'@Description "����������� ��������� � ������ ������ �� ����� �� ���."
Public Function ToAbc() As String
Attribute ToAbc.VB_Description = "����������� ��������� � ������ ������ �� ����� �� ���."
    ToAbc = IAbcElement_ToAbc
End Function
