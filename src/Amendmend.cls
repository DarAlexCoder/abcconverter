VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Amendmend"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "Rubberduck" ,"Predeclared Class Module"
'@IgnoreModule ProcedureNotUsed, WriteOnlyProperty
'@ModuleAttribute VB_Ext_KEY, "Rubberduck", "Predeclared Class Module"
'@Folder("Entities")
'@PredeclaredId
Option Explicit

Implements IAmendmend

Private Type TState
    Prefix As String
    Number As String
    Coefficient As IMathematics
    ExtraOperations As IAbcElement
End Type

Private This As TState

Public Function Create(ByVal Prefix As String, Optional ByVal Number As Long, Optional ByVal Coeff As Double) As IAmendmend
    Dim Result As Amendmend
    Set Result = New Amendmend
    
    With Result
        .Prefix = Prefix
        .Number = Number
        Set .Coefficients = Coefficient.Create(Coeff)
    End With
    
    Set Create = Result
End Function

Private Property Get IAmendmend_Prefix() As String
    IAmendmend_Prefix = This.Prefix
End Property
Public Property Let Prefix(ByVal Value As String)
    This.Prefix = Value
End Property

Private Property Get IAmendmend_Number() As String
    IAmendmend_Number = This.Number
End Property
Public Property Let Number(ByVal Value As String)
    This.Number = Value
End Property

Private Property Get IAmendmend_Coefficient() As IMathematics
    Set IAmendmend_Coefficient = This.Coefficient
End Property
Friend Property Set Coefficients(ByVal Coeff As IMathematics)
    GuardClauses.GuardNullReference Coeff
    
    Set This.Coefficient = Coeff
    Set This.ExtraOperations = This.Coefficient.Value
End Property

Private Function IAmendmend_ToString() As String
    Dim ExtraOps As String
    ExtraOps = This.ExtraOperations.ToAbc
    
    IAmendmend_ToString = "(" & This.Prefix & _
                          IIf(This.Number <> 0, This.Number, vbNullString) & _
                          IIf(0 < Len(ExtraOps), "." & ExtraOps, vbNullString) & ")"
End Function

Public Function Substraction() As IAmendmend
    Set Substraction = Create("��")
End Function
