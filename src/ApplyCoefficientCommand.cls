VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ApplyCoefficientCommand"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "��������� ����������� � ��������"
'@PredeclaredId
'@ModuleDescription "��������� ����������� � ��������"
'@Folder("Entities.Commands")
Option Explicit

Implements ICommand

Private Type TState
    Reciever As IUnitRate
    Coefficient As Double
End Type

Private This As TState

Public Function Create(ByVal Reciever As IUnitRate, ByVal Coefficient As Double) As ICommand
    Dim Command As ApplyCoefficientCommand
    Set Command = New ApplyCoefficientCommand
    
    Set Command.Reciever = Reciever
    Command.Coefficient = Coefficient
    
    Set Create = Command
End Function

Public Property Get Reciever() As IUnitRate
    Set Reciever = This.Reciever
End Property
Friend Property Set Reciever(ByVal Value As IUnitRate)
    GuardClauses.GuardNullReference Value
    Set This.Reciever = Value
End Property

Public Property Get Coefficient() As Double
    Coefficient = This.Coefficient
End Property
Friend Property Let Coefficient(ByVal Value As Double)
    This.Coefficient = Value
End Property

Private Sub ICommand_Execute(Optional ByVal Reciever As IUnitRate)
    GuardClauses.GuardDefaultInstance Me, ApplyCoefficientCommand, TypeName(Me)
    
    This.Reciever.Coefficients.Add This.Coefficient
    
    With This.Reciever.Amendmends
        .Add Amendmend.Create("�", 1, This.Coefficient)
        .Add Amendmend.Create("�", 41, This.Coefficient)
    End With
End Sub
