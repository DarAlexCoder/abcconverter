VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "CoefficientsDetector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "��������� ������� ������� � ���������� ������� ���������� ������������ � �������� ��������, ���� ������� �����������."
'@ModuleDescription "��������� ������� ������� � ���������� ������� ���������� ������������ � �������� ��������, ���� ������� �����������."
'@Folder("Entities.Detectors")
Option Explicit

Implements ICommandDetector

Private Type TState
    Detector As IDetector
    Command As ICommand
End Type

Private This As TState

Private Sub Class_Initialize()
    Set This.Detector = TagDetector.From( _
                        "*����������� ��������� ��������*")
End Sub

Private Function ICommandDetector_TryDetect(ByVal From As IEntry, ByVal Reciever As IUnitRate) As Boolean
    GuardNullReference Instance:=From, Source:=TypeName(Me)
    GuardNullReference Instance:=Reciever, Source:=TypeName(Me)
    
    If This.Detector.IsMatch(From.Text) Then
        Set This.Command = ApplyCoefficientCommand.Create(Reciever, From.Quantity)
    Else
        Set This.Command = Nothing
    End If
    
    ICommandDetector_TryDetect = Not (This.Command Is Nothing)
End Function

Private Property Get ICommandDetector_Command() As ICommand
    Set ICommandDetector_Command = This.Command
End Property
