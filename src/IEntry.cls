VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "IEntry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "������������ ���� ������ � ��������� ��������� �����."
'@ModuleDescription "������������ ���� ������ � ��������� ��������� �����."
Option Explicit

'@Folder("Entities")
'@Interface

'@Description "�������� �� ������� ""������������ ���� �����"""
Public Property Get Text() As String
Attribute Text.VB_Description = "�������� �� ������� ""������������ ���� �����"""
End Property

'@Description "�������� �� ������� ""������� ���������""."
Public Property Get Unit() As String
Attribute Unit.VB_Description = "�������� �� ������� ""������� ���������""."
End Property

'@Description "������������ ���� �����"
Public Property Get Code() As String
Attribute Code.VB_Description = "������������ ���� �����"
End Property

'@Description "�������� ������� ""����������"""
Public Property Get Quantity() As Double
Attribute Quantity.VB_Description = "�������� ������� ""����������"""
End Property
