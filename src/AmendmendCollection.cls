VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AmendmendCollection"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "������������ ��������� ��������"
'@ModuleDescription "������������ ��������� ��������"
'@Folder("Entities.Collections")
Option Explicit

Private Amendmends_ As Collection

Private Sub Class_Initialize()
    Set Amendmends_ = New Collection
End Sub

Private Sub Class_Terminate()
    Set Amendmends_ = Nothing
End Sub

'@DefaultMember
Public Property Get Item(ByRef Index As Variant) As IAmendmend
Attribute Item.VB_UserMemId = 0
    If TypeName(Index) = "Amendmend" Then
        Dim Key As IAmendmend
        Set Key = Index
        
        Set Item = Amendmends_.Item(Key.Prefix & Key.Number)
    Else
        Set Item = Amendmends_.Item(Index)
    End If
End Property

'@Enumerator
Public Property Get NewEnum() As IUnknown
Attribute NewEnum.VB_UserMemId = -4
    Set NewEnum = Amendmends_.[_NewEnum]
End Property

Public Sub Add(ByRef Item As IAmendmend)
    If Item Is Nothing Then Exit Sub
    
    Dim Key As String
    Key = Item.Prefix & Item.Number
    
    Amendmends_.Add Item, Key
End Sub

Public Function Count() As Long
    Count = Amendmends_.Count
End Function

Public Sub Remove(ByVal Index As Variant)
    Amendmends_.Remove Index
End Sub

Public Function Exists(ByVal Item As IAmendmend) As Boolean
    On Error GoTo Not_Exists
    
    If Item Is Nothing Then Exit Function
    Amendmends_.Item Item.Prefix & Item.Number
    
    Exists = True
    
Not_Exists:
End Function
