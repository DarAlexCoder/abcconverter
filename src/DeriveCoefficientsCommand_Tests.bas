Attribute VB_Name = "DeriveCoefficientsCommand_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

Private Assert As Object

Private Source_ As IUnitRate
Private Reciever_ As IUnitRate

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    Set Assert = CreateObject("Rubberduck.AssertClass")
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestInitialize
Private Sub TestInitialize()
    'This method runs before every test in the module..
    Set Source_ = UnitRate.Construct("����_01-01-01", "source", "unit", "12,345")
    Set Reciever_ = UnitRate.Construct("����_02-02-02", "reciever", "unit", "6,789")
End Sub

'@TestCleanup
Private Sub TestCleanup()
    'this method runs after every test in the module.
    Set Source_ = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_Nothing_DoesNothing()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As ICommand
    Set Sut = DeriveCoefficientsCommand.Create(Source_)

    'Act:
    Sut.Execute Nothing

    'Assert:
    Assert.Succeed

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_SourceWithCoefficients_CopiesCoefficientsAndAmendmends()
    On Error GoTo TestFail
    
    'Arrange:
    ApplyCoefficientCommand.Create(Source_, 1.04).Execute
    Dim Sut As ICommand
    Set Sut = DeriveCoefficientsCommand.Create(Source_)

    'Act:
    Sut.Execute Reciever_

    'Assert:
    Assert.AreEqual "�0202-02(�1.1,04)(�41.1,04)#�=1,04'6,789''+#reciever*", Reciever_.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
