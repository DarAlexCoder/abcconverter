VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "IUnitRate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'@Folder "Entities.Interfaces"
'@Interface

'@Description "�������� ��� ��������"
Public Property Get Code() As String
Attribute Code.VB_Description = "�������� ��� ��������"
End Property

'@Description "�������� ��������"
Public Property Get Description() As String
Attribute Description.VB_Description = "�������� ��������"
End Property

'@Description "�������� �������������� ����� � �������� ��������."
Public Sub Append(ByVal Clarification As String)
Attribute Append.VB_Description = "�������� �������������� ����� � �������� ��������."
End Sub

'@Description "������� ���������"
Public Property Get Unit() As String
Attribute Unit.VB_Description = "������� ���������"
End Property

'@Description "����������"
Public Property Get Quantity() As Double
Attribute Quantity.VB_Description = "����������"
End Property

'@Description "������������, ����������� � ��������."
Public Property Get Coefficients() As Collection
Attribute Coefficients.VB_Description = "������������, ����������� � ��������."
End Property

'@Description "��������, ����������� � ��������."
Public Property Get Amendmends() As AmendmendCollection
Attribute Amendmends.VB_Description = "��������, ����������� � ��������."
End Property

'@Description "����������� �������� � ������."
Public Function ToString() As String
Attribute ToString.VB_Description = "����������� �������� � ������."
End Function

'@Description "���������� � �������� ���������� �������������� ��������."
Public Sub Add(ByVal Value As Double)
Attribute Add.VB_Description = "���������� � �������� ���������� �������������� ��������."
End Sub

'@Description "�������� �� �������� ���������� �������� ��������."
Public Sub Subtract(ByVal Value As Double)
Attribute Subtract.VB_Description = "�������� �� �������� ���������� �������� ��������."
End Sub

'@Description "����� ������� ���������� �� �������� ��������."
Public Sub Divide(ByVal Value As Double)
Attribute Divide.VB_Description = "����� ������� ���������� �� �������� ��������."
End Sub

'@Description "�������� ������� ���������� �� �������� ��������."
Public Sub Multiply(ByVal Value As Double)
Attribute Multiply.VB_Description = "�������� ������� ���������� �� �������� ��������."
End Sub

'@Description "��������� �������� ������ � ��������. ��� ���� ��� ��������� �������� � �������� ������������ � ��������� ������ � ���� ������ ���."
Public Sub BindWith(ByVal Target As Range)
Attribute BindWith.VB_Description = "��������� �������� ������ � ��������. ��� ���� ��� ��������� �������� � �������� ������������ � ��������� ������ � ���� ������ ���."
End Sub

'@Description "�������� ����� � ������� ����� �������."
Public Sub Refresh()
Attribute Refresh.VB_Description = "�������� ����� � ������� ����� �������."
End Sub
