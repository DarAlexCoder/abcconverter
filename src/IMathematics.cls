VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "IMathematics"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "����������� �������������� ��������,"
'@ModuleDescription "����������� �������������� ��������,"
'@Folder("Entities.Interfaces")
Option Explicit

'@Description "���������� � �������� �������� ��������� ��������."
Public Sub Add(ByVal Value As Double)
Attribute Add.VB_Description = "���������� � �������� �������� ��������� ��������."
End Sub

'@Description "�������� �� �������� �������� ��������� ��������."
Public Sub Substract(ByVal Value As Double)
Attribute Substract.VB_Description = "�������� �� �������� �������� ��������� ��������."
End Sub

'@Description "�������� ������� �������� �� ��������� ��������."
Public Sub Multiply(ByVal Value As Double)
Attribute Multiply.VB_Description = "�������� ������� �������� �� ��������� ��������."
End Sub

'@Description "�������� �� �������� �������� ��������� ��������."
Public Sub Divide(ByVal Value As Double)
Attribute Divide.VB_Description = "�������� �� �������� �������� ��������� ��������."
End Sub

'@Description "���������� ������� �������� � ������������ ��������������� ����������."
Public Property Get Value() As IAbcElement
Attribute Value.VB_Description = "���������� ������� �������� � ������������ ��������������� ����������."
End Property
