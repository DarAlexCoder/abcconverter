Attribute VB_Name = "RibbonLoader"
Option Explicit

'@Folder("GUI")
Public Sub LoadCustRibbon()

    Dim hFile As Long
    Dim Path As String
    Dim fileName As String
    Dim ribbonXML As String
    Dim user As String


    hFile = FreeFile
    user = VBA.Environ$("Username")
    If user = "63001" Then user = "2402"
    Path = "C:\Users\" & user & "\AppData\Local\Microsoft\Office\"
    fileName = "Excel.officeUI"

    ribbonXML = "<mso:customUI      xmlns:mso='http://schemas.microsoft.com/office/2009/07/customui'>" & vbNewLine
    ribbonXML = ribbonXML + "  <mso:ribbon>" & vbNewLine
    ribbonXML = ribbonXML + "    <mso:qat/>" & vbNewLine
    ribbonXML = ribbonXML + "    <mso:tabs>" & vbNewLine
    ribbonXML = ribbonXML + "      <mso:tab id='reportTab' label='BP test' insertBeforeQ='mso:TabFormat'>" & vbNewLine
    ribbonXML = ribbonXML + "        <mso:group id='reportGroup' label='BP' autoScale='true'>" & vbNewLine
    ribbonXML = ribbonXML + "          <mso:button id='runReport' label='Convert' " & vbNewLine
    ribbonXML = ribbonXML + "imageMso='AppointmentColor3'      onAction='Handler'/>" & vbNewLine
    ribbonXML = ribbonXML + "        </mso:group>" & vbNewLine
    ribbonXML = ribbonXML + "      </mso:tab>" & vbNewLine
    ribbonXML = ribbonXML + "    </mso:tabs>" & vbNewLine
    ribbonXML = ribbonXML + "  </mso:ribbon>" & vbNewLine
    ribbonXML = ribbonXML + "</mso:customUI>"

    ribbonXML = Replace(ribbonXML, """", vbNullString)

    Open Path & fileName For Output Access Write As hFile
    Print #hFile, ribbonXML
    Close hFile

End Sub

Public Sub ClearCustRibbon()

    Dim hFile As Long
    Dim Path As String
    Dim fileName As String
    Dim ribbonXML As String
    Dim user As String


    hFile = FreeFile
    user = VBA.Environ$("Username")
    If user = "63001" Then user = "2402"
    Path = "C:\Users\" & user & "\AppData\Local\Microsoft\Office\"
    fileName = "Excel.officeUI"

    ribbonXML = "<mso:customUI           xmlns:mso=""http://schemas.microsoft.com/office/2009/07/customui"">" & _
                "<mso:ribbon></mso:ribbon></mso:customUI>"

    Open Path & fileName For Output Access Write As hFile
    Print #hFile, ribbonXML
    Close hFile

End Sub


