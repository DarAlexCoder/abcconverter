Attribute VB_Name = "Summator_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

#Const LateBind = LateBindTests

#If LateBind Then
    Private Assert As Object
#Else
    Private Assert As Rubberduck.AssertClass
    #End If

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    #If LateBind Then
        Set Assert = CreateObject("Rubberduck.AssertClass")
    #Else
        Set Assert = New Rubberduck.AssertClass
    #End If
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestMethod
Public Sub Add_NothingRate_DoesNotThrow()
On Error GoTo Test_Fail

    With New Summator
        .Add Nothing
    End With
    
    Assert.Succeed
    
    Exit Sub
    
Test_Fail:
    Assert.Fail
End Sub

'@TestMethod
Public Sub IsEmpty_IfNothingAdded_True()
    With New Summator
        Assert.IsTrue .IsEmpty
    End With
End Sub

'@TestMethod("Uncategorized")
Private Sub ToArray_WhenEmpty_EmptyArray()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Summator
    Set Sut = New Summator
    
    'Act:
    Dim Actual As Variant
    Actual = Sut.ToArray

    'Assert:
    Assert.IsTrue IsEmpty(Actual)

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub ToArray_NonEmpty_ContainSpecialWorks()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As Summator
    Set Sut = New Summator
    Sut.Add UnitRate.Construct("����_13-03-004-15", "��������", "�2", 63.35)
    
    'Act:
    Dim Actual As Variant
    Actual = Sut.ToArray

    'Assert:
    Assert.IsFalse IsEmpty(Actual)
    Assert.AreEqual "���������� ������*", Actual(1, 1)
    Assert.AreEqual "�1303-004-15'63,35''+#��������*", Actual(2, 1)

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
