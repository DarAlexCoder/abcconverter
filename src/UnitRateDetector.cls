VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "UnitRateDetector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "�������� ��������� ������� UnitRate, ���� ������ ������������� �������� ��������."
'@ModuleDescription "�������� ��������� ������� UnitRate, ���� ������ ������������� �������� ��������."
'@Folder("Entities.Detectors")
Option Explicit

Private Type TState
    Detector As IDetector
    Detected As IUnitRate
End Type

Private This As TState

Private Sub Class_Initialize()
    Set This.Detector = RegexDetector.From("(\d+\-?)+")
End Sub
'@Description "���������� True ��� ���������� ����������� ������� From; � ��������� ������ - False."
Public Function TryDetect(ByVal From As IEntry) As Boolean
Attribute TryDetect.VB_Description = "���������� True ��� ���������� ����������� ������� From; � ��������� ������ - False."
    GuardNullReference Instance:=From, Source:=TypeName(Me)
    
    If This.Detector.IsMatch(From.Code) Then
        Set This.Detected = UnitRate.Construct(From.Code, From.Text, From.Unit, From.Quantity)
    Else
        Set This.Detected = Nothing
    End If
    
    TryDetect = Not (This.Detected Is Nothing)
End Function

'@Description "���������� �������� (����� �����), ���������� � ���������� ��������� ������������ ���������. � ��������� ������ Nothing."
Public Property Get Detected() As IUnitRate
Attribute Detected.VB_Description = "���������� �������� (����� �����), ���������� � ���������� ��������� ������������ ���������. � ��������� ������ Nothing."
    Set Detected = This.Detected
End Property
