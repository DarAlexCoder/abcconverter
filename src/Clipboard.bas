Attribute VB_Name = "Clipboard"
Option Explicit
'@Folder("Tools")

Public Sub SetText(ByVal Text As String)
    With New MSForms.DataObject
        .SetText Text
        .PutInClipboard
    End With
End Sub
