VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "TagDetector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "���������� ������� �� � ������ �������� ����. � ����� ����������� ��������� ����� ������ * � ? (��. ����������� �� ��������� Like � VBA)."
'@IgnoreModule ProcedureNotUsed
'@PredeclaredId
'@ModuleDescription "���������� ������� �� � ������ �������� ����. � ����� ����������� ��������� ����� ������ * � ? (��. ����������� �� ��������� Like � VBA)."
'@Folder("Entities.Detectors")
Option Explicit
Option Compare Text

Implements IDetector

Private Type TState
    Tags() As Variant
End Type

Private This As TState

'@Description "������� ����� �������� ��������� �����."
Public Function From(ParamArray Tags() As Variant) As IDetector
Attribute From.VB_Description = "������� ����� �������� ��������� �����."

    Dim Result As TagDetector
    Set Result = New TagDetector
    
    Result.Tags = Tags
    
    Set From = Result
End Function

Public Property Get Tags() As Variant
    Tags = This.Tags
End Property
Friend Property Let Tags(ByVal Value As Variant)
    GuardExpression Throw:=Not IsArray(Value), Source:=TypeName(Me), Message:="Tags can be initialized only from an array of strings."
    
    This.Tags = Value
End Property

Private Function IDetector_IsMatch(ByVal Text As String) As Boolean
    GuardDefaultInstance Me, TagDetector, TypeName(Me)
    
    Dim Tag As Variant
    
    For Each Tag In This.Tags
        IDetector_IsMatch = Text Like Tag
        If IDetector_IsMatch Then Exit For
    Next
End Function
