VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "DeriveCoefficientsCommand"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "��������� ������������ � �������� ��� ������� �������� �� ���������."
'@PredeclaredId
'@ModuleDescription "��������� ������������ � �������� ��� ������� �������� �� ���������."
'@Folder("Entities.Commands")
Option Explicit

Implements ICommand

Private Type TState
    Source As IUnitRate
End Type

Private This As TState

Public Function Create(ByVal Source As IUnitRate) As ICommand
    GuardClauses.GuardNullReference Source
    
    Dim Command As DeriveCoefficientsCommand
    Set Command = New DeriveCoefficientsCommand
    
    Set Command.Source = Source
    
    Set Create = Command
End Function

Public Property Get Source() As IUnitRate
    Set Source = This.Source
End Property
Friend Property Set Source(ByVal Value As IUnitRate)
    Set This.Source = Value
End Property

Private Sub ICommand_Execute(Optional ByVal Reciever As IUnitRate)
    GuardClauses.GuardDefaultInstance Me, DeriveCoefficientsCommand, TypeName(Me)
    
    If Reciever Is Nothing Then Exit Sub
    
    With This.Source
        Dim Amendmend As IAmendmend
        For Each Amendmend In .Amendmends
            Reciever.Amendmends.Add Amendmend
        Next
        
        Dim Coefficient As Variant
        For Each Coefficient In .Coefficients
            Reciever.Coefficients.Add Coefficient
        Next
    End With
End Sub
