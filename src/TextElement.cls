VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "TextElement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "�����, ������� ����� ���� �������� ����� ��� ���������� �����������."
'@ModuleDescription "�����, ������� ����� ���� �������� ����� ��� ���������� �����������."
'@Folder("Entities")
'@PredeclaredId
Option Explicit

Implements ITextElement

Private Content_ As String

'@Description "���������� ������� ���������� ���������� ���������."
Private Property Get ITextElement_Content() As String
Attribute ITextElement_Content.VB_Description = "���������� ������� ���������� ���������� ���������."
    ITextElement_Content = Content_
End Property
Friend Property Let Content(ByVal Value As String)
    Content_ = Value
End Property

'@Description "��������� � ������ ���������� ����� ������."
Private Sub ITextElement_Extend(ByVal WithText As String)
Attribute ITextElement_Extend.VB_Description = "��������� � ������ ���������� ����� ������."
    If Len(WithText) = 0 Or _
       WithText Like "������������ ���� �����" Then Exit Sub
    
    Content_ = Content_ & " " & WithText
End Sub

'@Description "������� ��������� ������."
Public Function Create(ByVal Text As String) As ITextElement
Attribute Create.VB_Description = "������� ��������� ������."
    Dim Result As TextElement
    Set Result = New TextElement
    
    Result.Content = Text
    
    Set Create = Result
End Function
