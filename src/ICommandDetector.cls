VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ICommandDetector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "������������ �������� �������, ������� ������������ ��������� �������."
'@ModuleDescription "������������ �������� �������, ������� ������������ ��������� �������."
Option Explicit

'@Folder("Entities.Detectors")
'@Interface

'@Description "���������� True ��� ���������� ����������� ������� � IEntry; � ��������� ������ - False."
Public Function TryDetect(ByVal From As IEntry, ByVal Reciever As IUnitRate) As Boolean
Attribute TryDetect.VB_Description = "���������� True ��� ���������� ����������� ������� � IEntry; � ��������� ������ - False."
End Function

'@Description "���������� �������, ���������� � ���������� ��������� ������������ ���������. � ��������� ������ Nothing."
Public Property Get Command() As ICommand
Attribute Command.VB_Description = "���������� �������, ���������� � ���������� ��������� ������������ ���������. � ��������� ������ Nothing."
End Property
