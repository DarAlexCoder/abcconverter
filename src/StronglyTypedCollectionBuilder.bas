Attribute VB_Name = "StronglyTypedCollectionBuilder"
'@ModuleDescription "Creates a class named {CollectionClassName} that is a strongly typed collection of {ObjectClassName} objects."
'@Folder("Tools")
Option Explicit

'@Description "This procedure will create a class named {CollectionClassName} that is a strongly typed collection of {ObjectClassName} objects. WARNING: calling this procedure will overwrite any existing module named {CollectionClassName} Usage: BuildStronglyTypedCollectionClass ""Person"", ""People""."
Public Sub BuildStronglyTypedCollectionClass( _
    ObjectClassName As String, _
    CollectionClassName As String)
    
    Dim s As String
    s = s & "Attribute VB_GlobalNameSpace = False" & vbNewLine
    s = s & "Attribute VB_Creatable = False" & vbNewLine
    s = s & "Attribute VB_PredeclaredId = False" & vbNewLine
    s = s & "Attribute VB_Exposed = False" & vbNewLine
    s = s & "Option Compare Database" & vbNewLine
    s = s & "Option Explicit" & vbNewLine
    s = s & "" & vbNewLine
    s = s & "Private mCol As Collection" & vbNewLine
    s = s & "" & vbNewLine
    s = s & "" & vbNewLine
    s = s & "Private Sub Class_Initialize()" & vbNewLine
    s = s & "    Set mCol = New Collection" & vbNewLine
    s = s & "End Sub" & vbNewLine
    s = s & "" & vbNewLine
    s = s & "Private Sub Class_Terminate()" & vbNewLine
    s = s & "    Set mCol = Nothing" & vbNewLine
    s = s & "End Sub" & vbNewLine
    s = s & "" & vbNewLine
    s = s & "Property Get Item(Index As Variant) As " & ObjectClassName & "" & vbNewLine
    s = s & "Attribute Item.VB_UserMemId = 0" & vbNewLine
    s = s & "    'There is an attribute above this comment (hidden in the VBA IDE)" & vbNewLine
    s = s & "    '   that makes this property the default property for the class" & vbNewLine
    s = s & "    Set Item = mCol.Item(Index)" & vbNewLine
    s = s & "End Property" & vbNewLine
    s = s & "" & vbNewLine
    s = s & "Property Get NewEnum() As IUnknown" & vbNewLine
    s = s & "Attribute NewEnum.VB_UserMemId = -4" & vbNewLine
    s = s & "    'There is an attribute above this comment (hidden in the VBA IDE)" & vbNewLine
    s = s & "    '   that makes this property the default enumerator property for the class" & vbNewLine
    s = s & "    Set NewEnum = mCol.[_NewEnum]" & vbNewLine
    s = s & "End Property" & vbNewLine
    s = s & "" & vbNewLine
    s = s & "Public Sub Add(Item As " & ObjectClassName & ", Optional Key As Variant)" & vbNewLine
    s = s & "    mCol.Add Item, Key" & vbNewLine
    s = s & "End Sub" & vbNewLine
    s = s & "Public Function Count() As Long" & vbNewLine
    s = s & "    Count = mCol.Count" & vbNewLine
    s = s & "End Function" & vbNewLine
    s = s & "Public Sub Remove(Index As Variant)" & vbNewLine
    s = s & "    mCol.Remove Index" & vbNewLine
    s = s & "End Sub"
    
    
    'Save the generated code to a temporary file
    Dim fpTemp As String
    fpTemp = TempFileName()  'https://nolongerset.com/tempfilename/
    FileWrite fpTemp, s      'https://nolongerset.com/text-files-read-write-append/
    
    'Load the file
    ' NOTE: this overwrites any existing {CollectionClassName}
    '       class module with no warning
    LoadFromText acModule, CollectionClassName, fpTemp
    
    'Clean up the temporary file
    Kill fpTemp
    
End Sub

Public Sub CreateCollection()
    
End Sub

