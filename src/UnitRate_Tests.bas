Attribute VB_Name = "UnitRate_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder "Tests"

#Const LateBind = LateBindTests

#If LateBind Then
    Private Assert As Object
#Else
    Private Assert As Rubberduck.AssertClass
    #End If

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    #If LateBind Then
        Set Assert = CreateObject("Rubberduck.AssertClass")
    #Else
        Set Assert = New Rubberduck.AssertClass
    #End If
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestMethod
Public Sub Construct_WithAllArguments_AreNotSameWithUnitRate()
    On Error GoTo TestFail
    
    Dim Sut As IUnitRate
    
    Set Sut = UnitRate.Construct("01-01", "test", "unit", 10)
    
    Assert.AreNotSame UnitRate, Sut

    Set Sut = Nothing

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Code_CorrectValue_IsExtracted()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "test", "unit", 10)
        Assert.AreEqual "01-01", .Code
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Description_CorrectValue_IsExtracted()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "test", "unit", 10)
        Assert.AreEqual "test", .Description
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Description_WithPipesWeight_WeightExcluded()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "test ��� �����", "unit", 10)
        Assert.AreEqual "test", .Description
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Description_WithAsterics_AstericsExcluded()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "test*", "unit", 10)
        Assert.AreEqual "test", .Description
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Description_WithOrdinals_OrdinalsExcluded()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", " 1 test", "unit", 10)
        Assert.AreEqual "test", .Description
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Description_WithWhitespaces_WhitespacesSkipped()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "  test", "unit", 10)
        Assert.AreEqual "test", .Description
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Unit_CorrectValue_IsExtracted()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "test", "unit", 10)
        Assert.AreEqual "unit", .Unit
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Quantity_CorrectValue_IsExtracted()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "test", "unit", 10)
        Assert.AreEqual 10#, .Quantity
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Quantity_WithDotsValue_IsExtracted()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "test", "unit", 12.345)
        Assert.AreEqual 12.345, .Quantity
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Quantity_WithCommaValue_IsExtracted()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01", "test", "unit", 12.345)
        Assert.AreEqual 12.345, .Quantity
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_BuildWorls_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", 12.345)
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_BuildWorlsWithNegativeQuantity_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "-12,345")
        Assert.AreEqual "�0101-01(��)'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_BuildWorlsWithCoefficient_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Coefficients.Add 1.15
        Assert.AreEqual "�0101-01#�=1,15'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_BuildWorlsWithManyCoefficients_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Coefficients.Add 1.15
        .Coefficients.Add 1.25
        Assert.AreEqual "�0101-01#�=1,15#�=1,25'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Amendmends_ByDefault_IsNotNothing()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", 12.345)
        Assert.IsNotNothing .Amendmends
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Coefficients_ByDefault_IsNotNothing()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", 12.345)
        Assert.IsNotNothing .Coefficients
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterAdd_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Add 1.15
        Assert.AreEqual "�0101-01'12,345+1,15''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterAddZero_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Add 0#
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterSubtract_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Subtract 1.15
        Assert.AreEqual "�0101-01'12,345-1,15''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterSubtractZero_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Subtract 0#
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterMultiply_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Multiply 1.15
        Assert.AreEqual "�0101-01'12,345.1,15''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterMultiplyWithZero_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Multiply 0#
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterMultiplyWithOne_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Multiply 1#
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterDivide_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Divide 1.15
        Assert.AreEqual "�0101-01'12,345:1,15''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterDivideWithZero_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Divide 0#
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ToString_AfterDivideWithOne_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Divide 1#
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub ToString_WithComplexUnit_ConvertsQuantity()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "100 ��.", "12,345")
        .Divide 1#
        
        Assert.AreEqual "�0101-01'12,345.100''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Append_CorrectText_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Append "appendix"
        Assert.AreEqual "�0101-01'12,345''+#test appendix*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Append_vbNullString_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Append vbNullString
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Append_WrongText_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Append "������������ ���� �����"
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Append_NonSupportedText_EqualTo()
    On Error GoTo TestFail
    
    With UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
        .Append "  �����"
        Assert.AreEqual "�0101-01'12,345''+#test*", .ToString
    End With

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub ApplyCoefficient_AfterExecute_AppendsAmendmendsAndCoefficient()
    On Error GoTo TestFail
    
    Dim Sut As IUnitRate
    Set Sut = UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
    Dim ApplyCoefficient As ICommand
    Set ApplyCoefficient = ApplyCoefficientCommand.Create(Sut, 10)
    
    ApplyCoefficient.Execute
    
    Assert.AreEqual 2&, Sut.Amendmends.Count
    Assert.AreEqual 1&, Sut.Coefficients.Count

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
