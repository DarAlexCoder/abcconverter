VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ApplyTierCommand"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "��������� �������� �� ������ � ��������� �������"
'@IgnoreModule IntegerDataType
'@PredeclaredId
'@ModuleDescription "��������� �������� �� ������ � ��������� �������"
'@Folder("Entities.Commands")
Option Explicit

Implements ICommand

Private Type TState
    Reciever As IUnitRate
    Height As Double
    Tier As Byte
    Clarification As String
End Type

Private This As TState

Public Function Create(ByVal Reciever As IUnitRate, ByVal Height As Double, ByVal Tier As Byte, ByVal Text As String) As ICommand
    Dim Command As ApplyTierCommand
    Set Command = New ApplyTierCommand
    
    Set Command.Reciever = Reciever
    Command.Height = Height
    Command.Tier = Tier
    Command.Clarification = Text
    
    Set Create = Command
End Function

Public Property Get Reciever() As IUnitRate
    Set Reciever = This.Reciever
End Property
Friend Property Set Reciever(ByVal Value As IUnitRate)
    GuardClauses.GuardNullReference Value
    Set This.Reciever = Value
End Property

Public Property Get Height() As Double
    Height = This.Height
End Property
Friend Property Let Height(ByVal Value As Double)
    This.Height = Value
End Property

Public Property Get Tier() As Byte
    Tier = This.Tier
End Property
Friend Property Let Tier(ByVal Value As Byte)
    This.Tier = Value
End Property

Public Property Get Clarification() As String
    Clarification = This.Clarification
End Property
Friend Property Let Clarification(ByVal Value As String)
    This.Clarification = Value
End Property

Private Sub ICommand_Execute(Optional ByVal Reciever As IUnitRate)
    Dim Code As Integer
    
    If 1 < Tier And This.Height < 5 Then
        Code = 12017
    ElseIf This.Height > 5 Then
        Select Case Tier
            Case 1: Code = 12013
            Case 2: Code = 12014
            Case Is > 2: Code = 12015
        End Select
    Else
        Exit Sub
    End If
    
    This.Reciever.Amendmends.Add Amendmend.Create("�-�", Code)
    This.Reciever.Append This.Clarification
End Sub
