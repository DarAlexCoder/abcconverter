VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AbcMathematics"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "������������ �������������� ��������, ����������� �� �������� ���."
'@ModuleDescription "������������ �������������� ��������, ����������� �� �������� ���."
'@PredeclaredId
'@Folder("Entities.Abc")
Option Explicit

Implements IMathematics
Implements IAbcElement

Private ExtraOperations_ As String

'@Description "������� ��������� ������ AbcConverter."
Public Function Create(ByVal Value As String) As IMathematics
Attribute Create.VB_Description = "������� ��������� ������ AbcConverter."
    Dim Result As AbcMathematics
    Set Result = New AbcMathematics
    
    Result.CurrentValue = Value
    Set Create = Result
End Function

Public Property Get CurrentValue() As String
    CurrentValue = ExtraOperations_
End Property
Friend Property Let CurrentValue(ByVal Value As String)
    ExtraOperations_ = Value
End Property

Private Sub IMathematics_Add(ByVal Value As Double)
    If Value = 0# Then Exit Sub
    ExtraOperations_ = ExtraOperations_ & "+" & Value
End Sub

Private Sub IMathematics_Substract(ByVal Value As Double)
    If Value = 0# Then Exit Sub
    ExtraOperations_ = ExtraOperations_ & "-" & Value
End Sub

Private Sub IMathematics_Multiply(ByVal Value As Double)
    If Value = 0# Or Value = 1# Then Exit Sub
    ExtraOperations_ = ExtraOperations_ & "." & Value
End Sub

Private Sub IMathematics_Divide(ByVal Value As Double)
    If Value = 0# Or Value = 1# Then Exit Sub
    ExtraOperations_ = ExtraOperations_ & ":" & Value
End Sub

Private Property Get IMathematics_Value() As IAbcElement
    Set IMathematics_Value = Me
End Property

Private Function IAbcElement_ToAbc() As String
    IAbcElement_ToAbc = ExtraOperations_
End Function
