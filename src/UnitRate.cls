VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "UnitRate"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'@IgnoreModule WriteOnlyProperty
'@PredeclaredId
'@Folder "Entities"

Option Explicit
Option Compare Text

Implements IUnitRate

Private Type TState
    AbcMathematics As IMathematics
    ExtraOperations As IAbcElement
    ConvertUnit As IAbcElement
End Type

Private This As TState

Private Code_ As IAbcGuid
Private Description_ As String
Private Unit_ As String
Private Quantity_ As Double
Private Amendmends_ As AmendmendCollection
Private Coefficients_ As Collection
Private Binding_ As Range

Private Sub Class_Initialize()
    Set Amendmends_ = New AmendmendCollection
    Set Coefficients_ = New Collection
    Set This.AbcMathematics = New AbcMathematics
    Set This.ExtraOperations = This.AbcMathematics.Value
End Sub
Private Sub Class_Terminate()
    Set Amendmends_ = Nothing
    Set Coefficients_ = Nothing
End Sub

Private Property Get IUnitRate_Code() As String
    IUnitRate_Code = Code_.Code
End Property
Public Property Let Code(ByVal Value As String)
    Set Code_ = AbcGuid.From(Value)
    UpdateBinding
End Property

Private Property Get IUnitRate_Description() As String
    IUnitRate_Description = Description_
End Property
Public Property Let Description(ByVal Value As String)
    Description_ = Replace(Value, " ��� �����", vbNullString)
    Description_ = Replace(Description_, "*", vbNullString)
    Description_ = RegEx.Replace(Description_, "^\s*\d+(\.\s*\d+)?\s+")
    Description_ = WorksheetFunction.Trim(Description_)
    
    UpdateBinding
End Property

Private Sub IUnitRate_Append(ByVal Clarification As String)
    If 0 = Len(Clarification) Then Exit Sub
    If Clarification Like "*�������*���*�����*" Then Exit Sub
    If Clarification Like "*�����" Then Exit Sub
    
    Description_ = Description_ & " " & Clarification
    
    UpdateBinding
End Sub

Public Function Construct(ByVal Code As String, ByVal Description As String, ByVal Unit As String, ByVal Quantity As Double) As IUnitRate
    Dim Result As UnitRate
    Set Result = New UnitRate
    
    With Result
        .Code = Code
        .Description = Description
        .Unit = Unit
        .Quantity = Quantity
    End With
    
    Set Construct = Result
End Function

Private Property Get IUnitRate_Unit() As String
    IUnitRate_Unit = Unit_
End Property
Public Property Let Unit(ByVal Value As String)
    Unit_ = Value
    Set This.ConvertUnit = AbcUnit.From(Value).Convert
    UpdateBinding
End Property

Private Property Get IUnitRate_Quantity() As Double
    IUnitRate_Quantity = Quantity_
End Property
Public Property Let Quantity(ByVal Value As Double)
    Quantity_ = Value
    If (Quantity_ < 0) Then Amendmends_.Add Amendmend.Substraction()
    UpdateBinding
End Property

Private Property Get IUnitRate_Coefficients() As Collection
    Set IUnitRate_Coefficients = Coefficients_
End Property

Private Property Get IUnitRate_Amendmends() As AmendmendCollection
    Set IUnitRate_Amendmends = Amendmends_
End Property

Private Sub IUnitRate_Add(ByVal Value As Double)
    This.AbcMathematics.Add Value
    UpdateBinding
End Sub

Private Sub IUnitRate_Subtract(ByVal Value As Double)
    This.AbcMathematics.Substract Value
    UpdateBinding
End Sub

Private Sub IUnitRate_Multiply(ByVal Value As Double)
    This.AbcMathematics.Multiply Value
    UpdateBinding
End Sub

Private Sub IUnitRate_Divide(ByVal Value As Double)
    This.AbcMathematics.Divide Value
    UpdateBinding
End Sub

Private Function IUnitRate_ToString() As String
    If Code_ Is Nothing Then Exit Function
    
    IUnitRate_ToString = Code_.ToString & Amendmends & Coefficients & "'" & Abs(Quantity_) & This.ConvertUnit.ToAbc & This.ExtraOperations.ToAbc & "''+#" & Description_ & "*"
End Function

Private Function Amendmends() As String
    Dim Amendmend As IAmendmend
    
    For Each Amendmend In Amendmends_
        Amendmends = Amendmends & Amendmend.ToString()
    Next
End Function

Private Function Coefficients() As String
    If Coefficients_.Count = 0 Then Exit Function
    
    Dim Coefficient As Variant
    For Each Coefficient In Coefficients_
        Coefficients = Coefficients & "#�=" & Coefficient
    Next
    
    Coefficients = "#�=" & Replace(Coefficients, "#�=", vbNullString, Count:=1)
End Function

Private Sub IUnitRate_BindWith(ByVal Target As Range)
    Set Binding_ = Target
    UpdateBinding
End Sub

Private Sub UpdateBinding()
    If Binding_ Is Nothing Then Exit Sub
    Binding_.Value2 = IUnitRate_ToString()
End Sub

Private Sub IUnitRate_Refresh()
    UpdateBinding
End Sub
