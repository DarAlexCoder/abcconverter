VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "It"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
'@Folder("Entities")
Option Explicit

Private Type TState
    CommandDetectors As Collection
    Command As ICommand
    UnitRateDetectors As UnitRateDetector
End Type

Private This As TState

Private Sub Class_Initialize()
    Set This.CommandDetectors = New Collection
    
    RegisterDetectors
End Sub

Private Sub RegisterDetectors()
    This.CommandDetectors.Add New AddQuantityCommandDetector
    This.CommandDetectors.Add New ApplyCoefficientCommandDetector
    This.CommandDetectors.Add New DeriveCoeficientCommandDetector
    This.CommandDetectors.Add New ApplyHeightCommandDetector
    This.CommandDetectors.Add New ApplyTierCommandDetector
End Sub

Public Function IsUnitRate(ByVal Row As IEntry) As Boolean
    IsUnitRate = This.UnitRateDetectors.TryDetect(From:=Row)
End Function

Public Property Get DetectedUnitRate() As IUnitRate
    Set DetectedUnitRate = This.UnitRateDetectors.Detected
End Property

Public Function IsCommand(ByVal Row As IEntry) As Boolean
    Dim CommandDetector As ICommandDetector
    
    For Each CommandDetector In This.CommandDetectors
        IsCommand = CommandDetector.TryDetect(From:=Row, Reciever:=DetectedUnitRate)
        If (Not IsCommand) Then GoTo Continue
        
        Set This.Command = CommandDetector.Command
Continue:
    Next
End Function

Public Property Get DetectedCommand() As ICommand
    Set DetectedCommand = This.Command
End Property
