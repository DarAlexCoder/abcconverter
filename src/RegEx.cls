VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "RegEx"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "������ � ����������� �����������"
'@ModuleDescription "������ � ����������� �����������"
'@PredeclaredId
'@Folder "Text"
Option Explicit

Private RegEx_ As RegExp

Private Sub Class_Initialize()
    Set RegEx_ = New RegExp
    With RegEx_
        .Global = True
        .IgnoreCase = True
        .MultiLine = True
    End With
End Sub

Private Sub Class_Terminate()
    Set RegEx_ = Nothing
End Sub

Public Function IsMatch(ByVal Text As String, ByVal Pattern As String) As Boolean
    RegEx_.Pattern = Pattern
    IsMatch = RegEx_.Test(Text)
End Function

Public Function Match(ByVal Text As String, ByVal Pattern As String) As String
    If (IsMatch(Text, Pattern)) Then
        Match = RegEx_.Execute(Text).Item(0)
    Else
        Match = vbNullString
    End If
End Function

Public Function GroupValue(ByVal Text As String, ByVal Pattern As String, ByVal GroupIndex As Long) As String
    If (IsMatch(Text, Pattern)) Then
        GroupValue = RegEx_.Execute(Text).Item(0).SubMatches.Item(GroupIndex)
    Else
        GroupValue = vbNullString
    End If
End Function

Public Function Replace(ByVal Text As String, ByVal Pattern As String, Optional ByVal ReplaceWith As String = vbNullString) As String
    RegEx_.Pattern = Pattern
    
    Replace = RegEx_.Replace(Text, ReplaceWith)
End Function
