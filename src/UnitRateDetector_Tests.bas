Attribute VB_Name = "UnitRateDetector_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

Private Assert As Object
Private Sut_ As UnitRateDetector

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    Set Assert = CreateObject("Rubberduck.AssertClass")
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestInitialize
Private Sub TestInitialize()
    'This method runs before every test in the module..
    Set Sut_ = New UnitRateDetector
End Sub

'@TestCleanup
Private Sub TestCleanup()
    'this method runs after every test in the module.
    Set Sut_ = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub TryDetect_CorrectEntry_IsTrue()
    Assert.IsTrue TryDetect_Helper("�����37-01-013-1")
End Sub

Private Function TryDetect_Helper(ByVal Code As String) As Boolean
    On Error GoTo TestFail
    
    Dim CorrectEntry As IEntry

    Set CorrectEntry = Entry.Create(Code, "Name", "unit", 10#)
    TryDetect_Helper = Sut_.TryDetect(From:=CorrectEntry)

TestExit:
    Set CorrectEntry = Nothing
    Exit Function
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Function

'@TestMethod("Uncategorized")
Private Sub TryDetect_WrongEntry_IsFalse()
    Assert.IsFalse TryDetect_Helper("�����")
End Sub

'@TestMethod("Uncategorized")
Private Sub Detected_CorrectEntry_IsNotNothing()
    Assert.IsTrue TryDetect_Helper("�����37-01-013-1")
    Assert.IsNotNothing Sut_.Detected
End Sub

'@TestMethod("Uncategorized")
Private Sub Detected_WrongEntry_IsNothing()
    Assert.IsFalse TryDetect_Helper("�����")
    Assert.IsNothing Sut_.Detected
End Sub
