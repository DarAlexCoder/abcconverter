Attribute VB_Name = "RegexDetector_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

Private Assert As Object
Private Sut_ As IDetector

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    Set Assert = CreateObject("Rubberduck.AssertClass")
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestInitialize
Private Sub TestInitialize()
    'This method runs before every test in the module..
    Set Sut_ = RegexDetector.From("\d+")
End Sub

'@TestCleanup
Private Sub TestCleanup()
    'this method runs after every test in the module.
    Set Sut_ = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub IsMatch_EmptyString_IsFalse()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Actual As Boolean

    'Act:
    Actual = Sut_.IsMatch(vbNullString)

    'Assert:
    Assert.IsFalse Actual

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub IsMatch_NonNumericString_IsFalse()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Actual As Boolean

    'Act:
    Actual = Sut_.IsMatch("non numbers in string")

    'Assert:
    Assert.IsFalse Actual

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub IsMatch_NumericString_IsTrue()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Actual As Boolean

    'Act:
    Actual = Sut_.IsMatch("123456789")

    'Assert:
    Assert.IsTrue Actual

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
