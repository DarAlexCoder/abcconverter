VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ITextElement"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Option Explicit

'@Folder("Entities.Interfaces")
'@Interface

'@Description "���������� ������� �����."
Public Property Get Content() As String
Attribute Content.VB_Description = "���������� ������� �����."
End Property

'@Description "��������� � �������� �������������� �����. "
Public Sub Extend(ByVal WithText As String)
Attribute Extend.VB_Description = "��������� � �������� �������������� �����. "
End Sub
