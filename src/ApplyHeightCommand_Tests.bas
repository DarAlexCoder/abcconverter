Attribute VB_Name = "ApplyHeightCommand_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

Private Assert As Object

Private Reciever_ As IUnitRate

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    Set Assert = CreateObject("Rubberduck.AssertClass")
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestInitialize
Private Sub TestInitialize()
    'This method runs before every test in the module..
    Set Reciever_ = UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
End Sub

'@TestCleanup
Private Sub TestCleanup()
    'this method runs after every test in the module.
    Set Reciever_ = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_WithHeightSmallThanFive_NotAppendsCoefficient()
    NoChanges_Helper Height:=3
End Sub

Private Sub NoChanges_Helper(ByVal Height As Double)
On Error GoTo TestFail
    
    Dim Command As ICommand

    Set Command = ApplyHeightCommand.Create(Reciever_, Height, Text:=vbNullString)
    Command.Execute
    
    Assert.AreEqual 0&, Reciever_.Amendmends.Count
    Assert.AreEqual "�0101-01'12,345''+#test*", Reciever_.ToString()

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_WithHeightEqualsToFive_NotAppendsCoefficient()
    NoChanges_Helper Height:=5
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_HeightGreatherThanFive_AppendsCoefficient()
    On Error GoTo TestFail
    
    Dim Command As ICommand

    Set Command = ApplyHeightCommand.Create(Reciever_, Height:=6, Text:="ext")
    Command.Execute
    
    Assert.AreEqual 1&, Reciever_.Amendmends.Count
    Assert.AreEqual "�0101-01(�1.1,04)#�=1,04'12,345''+#test ext*", Reciever_.ToString()

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_HeightGreatherThanFiveWithExistentAmendmendAndCoefficient_AppendsCoefficient()
    On Error GoTo TestFail
    
    Dim Command As ICommand
    Set Command = ApplyCoefficientCommand.Create(Reciever_, 1.2)
    Command.Execute
    
    Set Command = ApplyHeightCommand.Create(Reciever_, Height:=6, Text:="ext")
    Command.Execute
    
    Assert.AreEqual 2&, Reciever_.Amendmends.Count
    Assert.AreEqual "�0101-01(�1.1,2.1,04)(�41.1,2)#�=1,2#�=1,04'12,345''+#test ext*", Reciever_.ToString()

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
