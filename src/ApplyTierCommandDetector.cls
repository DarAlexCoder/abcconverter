VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ApplyTierCommandDetector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "������� ������� ApplyTierCommand, ���� ��������� ������������� �������� ��������."
'@ModuleDescription "������� ������� ApplyTierCommand, ���� ��������� ������������� �������� ��������."
'@Folder("Entities.Detectors")
Option Explicit

Implements ICommandDetector

Private Type TState
    Parent As RegexDetector
    Command As ICommand
End Type

Private This As TState

Private Sub Class_Initialize()
    Set This.Parent = RegexDetector.From("h=(\d+(.|,)?\d*)\s*�=(\d+(.|,)?\d*)$")
End Sub

Private Function ICommandDetector_TryDetect(ByVal From As IEntry, ByVal Reciever As IUnitRate) As Boolean
    GuardNullReference Instance:=From, Source:=TypeName(Me)
    GuardNullReference Instance:=Reciever, Source:=TypeName(Me)
    
    If Detector.IsMatch(From.Text) Then
        Set This.Command = ApplyTierCommand.Create( _
                           Reciever, _
                           Height:=ToDouble(This.Parent.Value(0, From.Text)), _
                           Tier:=ToDouble(This.Parent.Value(2, From.Text)), _
                           Text:=From.Text)
    Else
        Set This.Command = Nothing
    End If
    
    ICommandDetector_TryDetect = Not (This.Command Is Nothing)
End Function

Private Property Get Detector() As IDetector
    Set Detector = This.Parent
End Property

Private Property Get ICommandDetector_Command() As ICommand
    Set ICommandDetector_Command = This.Command
End Property
