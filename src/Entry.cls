VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Entry"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "������������ ���� ������ � ��������� ��������� �����."
'@ModuleDescription "������������ ���� ������ � ��������� ��������� �����."
'@PredeclaredId
'@Folder("Entities")
Option Explicit

Implements IEntry

Private Type TState
    Text As String
    Unit As String
    Code As String
    Quantity As Double
End Type

Private This As TState

Public Function Create(ByVal Code As String, ByVal Text As String, ByVal Unit As String, ByVal Quantity As Double) As IEntry
    Dim Result As Entry
    Set Result = New Entry
    
    Result.Code = Code
    Result.Text = Text
    Result.Unit = Unit
    Result.Quantity = Quantity
    
    Set Create = Result
End Function

Private Property Get IEntry_Text() As String
    IEntry_Text = This.Text
End Property
Friend Property Let Text(ByVal Value As String)
    This.Text = Value
End Property

Private Property Get IEntry_Unit() As String
    IEntry_Unit = This.Unit
End Property
Friend Property Let Unit(ByVal Value As String)
    This.Unit = Value
End Property

Private Property Get IEntry_Code() As String
    IEntry_Code = This.Code
End Property
Friend Property Let Code(ByVal Value As String)
    This.Code = Value
End Property

Private Property Get IEntry_Quantity() As Double
    IEntry_Quantity = This.Quantity
End Property
Friend Property Let Quantity(ByVal Value As Double)
    This.Quantity = Value
End Property

Public Function From(ByVal Row As Range) As IEntry
    GuardNullReference Instance:=Row, Source:=TypeName(Me)
    GuardExpression Throw:=Row.Rows.Value <> 1, Source:=TypeName(Me), Message:="Entry can be constructed from single row range only."
    
    Dim Result As Entry
    Set Result = New Entry
    
    With Row.EntireRow
        Result.Code = .Cells.Item(7).Text
        Result.Text = .Cells.Item(5).Text
        Result.Unit = .Cells.Item(6).Text
        Result.Quantity = .Cells.Item(8).Value2
    End With
    
    Set From = Result
End Function
