Attribute VB_Name = "ApplyTierCommand_Tests"
'@IgnoreModule IntegerDataType
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

#Const LateBind = LateBindTests

#If LateBind Then
    Private Assert As Object
#Else
    Private Assert As Rubberduck.AssertClass
#End If

Private Reciever_ As IUnitRate

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    #If LateBind Then
        Set Assert = CreateObject("Rubberduck.AssertClass")
    #Else
        Set Assert = New Rubberduck.AssertClass
    #End If
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestInitialize
Private Sub TestInitialize()
    'This method runs before every test in the module.
    Set Reciever_ = UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
End Sub

'@TestCleanup
Private Sub TestCleanup()
    'this method runs after every test in the module.
    Set Reciever_ = Nothing
End Sub

'@TestMethod
Public Sub Execute_WithSmallHeightAndTierBiggerThanTwo_AppendsSpecialAmmendmend()
    Execute_TestHelper Height:=3, Tier:=3, Text:="ext", Expected:="�0101-01(�-�12017)'12,345''+#test ext*"
End Sub

Private Sub Execute_TestHelper(ByVal Height As Integer, ByVal Tier As Integer, ByVal Text As String, ByVal Expected As String)
    On Error GoTo TestFail
    
    Dim Command As ICommand
    
    Set Command = ApplyTierCommand.Create(Reciever_, Height, Tier, Text)
    Command.Execute
    
    Assert.AreEqual 1&, Reciever_.Amendmends.Count
    Assert.AreEqual Expected, Reciever_.ToString()

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod
Public Sub Execute_WithHeightBiggerThanFiveAndOneTier_AppendsSpecialAmmendmend()
    Execute_TestHelper Height:=6, Tier:=1, Text:="ext", Expected:="�0101-01(�-�12013)'12,345''+#test ext*"
End Sub

'@TestMethod
Public Sub Execute_WithHeightBiggerThanFiveAndTwoTiers_AppendsSpecialAmmendmend()
    Execute_TestHelper Height:=6, Tier:=2, Text:="ext", Expected:="�0101-01(�-�12014)'12,345''+#test ext*"
End Sub

'@TestMethod
Public Sub Execute_WithHeightBiggerThanFiveAndThreeTiers_AppendsSpecialAmmendmend()
    Execute_TestHelper Height:=6, Tier:=3, Text:="ext", Expected:="�0101-01(�-�12015)'12,345''+#test ext*"
End Sub

'@TestMethod
Public Sub Execute_WithHeightBiggerThanFiveAndSevenTiers_AppendsSpecialAmmendmend()
    Execute_TestHelper Height:=6, Tier:=7, Text:="ext", Expected:="�0101-01(�-�12015)'12,345''+#test ext*"
End Sub

'@TestMethod
Public Sub Execute_WithHeightSmallerThanFiveAndOneTier_NoAmendmendWasAppend()
    On Error GoTo TestFail
    
    Dim Command As ICommand
    
    Set Command = ApplyTierCommand.Create(Reciever_, Height:=3, Tier:=1, Text:="ext")
    Command.Execute
    
    Assert.AreEqual 0&, Reciever_.Amendmends.Count
    Assert.AreEqual "�0101-01'12,345''+#test*", Reciever_.ToString()

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
