VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AddQuantityCommand"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "������� �������� ����� �������� � ������� ��� �����������"
'@PredeclaredId
'@ModuleDescription "������� �������� ����� �������� � ������� ��� �����������"
'@Folder("Entities.Commands")
Option Explicit

Implements ICommand

Private Type TState
    Quantity As Double
End Type

Private This As TState

Private Sub ICommand_Execute(Optional ByVal Reciever As IUnitRate)
    If This.Quantity = 0# Then Exit Sub
    If Reciever Is Nothing Then Exit Sub
    
    Reciever.Add This.Quantity
End Sub

Public Function Create(ByVal Quantity As Double) As ICommand
    Dim Result As AddQuantityCommand
    Set Result = New AddQuantityCommand
    
    Result.Quantity = Quantity
    
    Set Create = Result
End Function

Public Property Get Quantity() As Double
    Quantity = This.Quantity
End Property
Public Property Let Quantity(ByVal Value As Double)
    This.Quantity = Value
End Property
