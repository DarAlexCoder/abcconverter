Attribute VB_Name = "TextElement_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

#Const LateBind = LateBindTests

#If LateBind Then
    Private Assert As Object
#Else
    Private Assert As Rubberduck.AssertClass
#End If

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    #If LateBind Then
        Set Assert = CreateObject("Rubberduck.AssertClass")
    #Else
        Set Assert = New Rubberduck.AssertClass
    #End If
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub Extend_NullString_TextHasNoChanges()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As ITextElement
    Set Sut = TextElement.Create("test")

    'Act:
    Sut.Extend vbNullString

    'Assert:
    Assert.AreEqual "test", Sut.Content

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Extend_WithSpecialString_TextHasNoChanges()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As ITextElement
    Set Sut = TextElement.Create("test")

    'Act:
    Sut.Extend "������������ ���� �����"

    'Assert:
    Assert.AreEqual "test", Sut.Content

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Extend_WithSimpleText_TextAppendedBySpace()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As ITextElement
    Set Sut = TextElement.Create("test")

    'Act:
    Sut.Extend "with simple text"

    'Assert:
    Assert.AreEqual "test with simple text", Sut.Content

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Description_AfterCreateWithNonEmptyText_NonEmpty()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As ITextElement

    'Act:
    Set Sut = TextElement.Create("test")

    'Assert:
    Assert.AreEqual "test", Sut.Content

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
