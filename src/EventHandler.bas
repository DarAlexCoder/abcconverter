Attribute VB_Name = "EventHandler"
'@Folder("GUI")
Option Explicit
Option Compare Text

'Public Sub ConvertToAbc()
'    Dim RowEntry As IEntry
'    Dim CurrentRate As IUnitRate
'    Dim Row As Range
'
'    For Each Row In ActiveSheet.UsedRange.Rows
'        If Row.EntireRow.Hidden Then GoTo Continue
'
'        Set RowEntry = Entry.From(Row)
'
'        If This.IsUnitRate(RowEntry) Then
'            Set CurrentRate = This.UnitRate
'        ElseIf thisis.Command(Row) Then
'        ElseIf thisis.Header(Row) Then
'        Else
'
'        End If
'
'Continue:
'    Next
'End Sub

'@EntryPoint
Public Sub Handler()
    Dim CurrentRate As IUnitRate
    Dim LastUsedRate As IUnitRate
    Dim Code As Range
    Dim Name As String
    Dim Row As Range
    Dim RowIndex As Long
    Dim Quantity As Double
    
    Dim SpecialWorks As Summator
    Set SpecialWorks = New Summator
    
    For Each Row In ActiveSheet.UsedRange.Rows
        If Row.EntireRow.Hidden Then GoTo Continue
        
        Set Code = Row.EntireRow.Cells.Item(7)
        Name = Code.Offset(0, -2).Text
        
        If IsCaption(Name) And Not IsUnitRate(Code.Text) Then
            Row.Parent.Cells(Row.Row, 9).Value2 = "�2" & RegEx.Replace(Name, "^\s*\d+(\.\s*\d+)\s*") & "*"
            Set CurrentRate = Nothing
            GoTo Continue
        End If
        
        If IsUnitRate(Code.Text) Then
            Set CurrentRate = UnitRate.Construct( _
                              Code:=Code.Text, _
                              Description:=Code.Offset(0, -2).Text, _
                              Unit:=Code.Offset(0, -1).Text, _
                              Quantity:=Code.Offset(0, 1).Value2)
            
            If IsSpecialWorks(CurrentRate) Then
                ' ��������������� ������ ���������. ��������� �� �� ����������.
                SpecialWorks.Add CurrentRate
            Else
                If (Name Like "*�������� �� ������� �����*") Then
                    DeriveCoefficientsCommand.Create(LastUsedRate).Execute CurrentRate
                    CurrentRate.Refresh
                End If
                
                ' ��������� ����� ��������� ���������� � ������������ ������.
                CurrentRate.BindWith Row.Parent.Cells(Row.Row, 9)
                Set LastUsedRate = CurrentRate
            End If
        Else
            If Not (LastUsedRate Is Nothing) Then
                ' �������� ����� ������� �������������
                If (Name Like "*������ �������������") Or (Name Like "*�������") Then
                    AddQuantityCommand.Create(Code.Offset(0, 1).Value2).Execute LastUsedRate
                    Set CurrentRate = Nothing
                End If
                
                ' �������� ����������� ��������� ��������.
                If (Name Like "*����������� ��������� ��������*") Then
                    Quantity = Code.Offset(0, 1).Value2
                    ApplyCoefficientCommand.Create(LastUsedRate, Quantity).Execute
                    LastUsedRate.Refresh
                End If
            End If
            
            If CurrentRate Is Nothing Then GoTo Continue
            
            ' ��������� �������������� ����� � ��������.
            CurrentRate.Append Name
        End If
        
Continue:
        RowIndex = Row.Row
    Next
    
    ' ������� ������ "��������� ������" ��� �������������.
    If Not SpecialWorks.IsEmpty Then
        ActiveSheet.Range("I" & RowIndex & ":I" & RowIndex + SpecialWorks.Count).Value2 = SpecialWorks.ToArray()
    End If
    
    ' ����������� ���������� ����� � ����� ������
    ActiveSheet.UsedRange.Columns("I:I").Select
    CopyForAbc
End Sub

Private Function IsCaption(ByVal Text As String) As Boolean
    IsCaption = False
    'IsCaption = RegEx.IsMatch(Text, "^\s*\d+(\.\s*\d+)?\s*")
End Function

Private Function IsUnitRate(ByVal Code As String) As Boolean
    IsUnitRate = RegEx.IsMatch(Code, "(\d+\-?)+")
End Function

Public Function IsSpecialWorks(ByVal Rate As IUnitRate) As Boolean
    Dim Name As String
    Name = Rate.Description
    
    IsSpecialWorks = _
                   (Name Like "*�������*������*") Or _
                   (Name Like "*�����������*") Or _
                   (Name Like "*�������������*") Or _
                   (Name = "��������")
End Function

Public Sub CopyForAbc()
    Dim Cell As Range
    Dim List As String: List = vbNullString
    
    For Each Cell In Selection.SpecialCells(xlCellTypeVisible).Cells
        If Cell.Text <> vbNullString Then
            List = List & vbCr & Cell.Text
        End If
    Next
    
    If 0 < VBA.Len(List) Then
        Clipboard.SetText List
        Shell "X:\OSIPOSApps\CanCD\Scripts\PutToAbc.exe"
    Else
        MsgBox "�� ������� �� ����� ������ � ������� ��� ���.", vbCritical
    End If
End Sub


