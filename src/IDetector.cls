VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "IDetector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "������������ ����� ��� ����������� ���������� ������������ � ������."
'@ModuleDescription "������������ ����� ��� ����������� ���������� ������������ � ������."
Option Explicit

'@Folder("Entities.Detectors")
'@Interface

'@Description "���������� True, ���� � �������� ������ ������� ������������; � ��������� ������ - False."
Public Function IsMatch(ByVal Text As String) As Boolean
Attribute IsMatch.VB_Description = "���������� True, ���� � �������� ������ ������� ������������; � ��������� ������ - False."
End Function
