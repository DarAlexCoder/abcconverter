VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "RegexDetector"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Description = "���������� ������������� �� ������ ��������� ������� ����������� ���������."
'@IgnoreModule ProcedureNotUsed
'@PredeclaredId
'@ModuleDescription "���������� ������������� �� ������ ��������� ������� ����������� ���������."
'@Folder("Entities.Detectors")
Option Explicit

Implements IDetector

Private Type TState
    Pattern As String
End Type

Private This As TState

'@Description "������� ����� �������� ��������� ������� ����������� ���������."
Public Function From(ByVal Pattern As String) As IDetector
Attribute From.VB_Description = "������� ����� �������� ��������� ������� ����������� ���������."

    Dim Result As RegexDetector
    Set Result = New RegexDetector
    
    Result.Pattern = Pattern
    
    Set From = Result
End Function

'@Description "���������� ������ ����������� ��������� ��� �������. "
Public Property Get Pattern() As String
Attribute Pattern.VB_Description = "���������� ������ ����������� ��������� ��� �������. "
    Pattern = This.Pattern
End Property
'@Description "������������� ������ ����������� ��������� ��� �������."
Friend Property Let Pattern(ByVal Value As String)
Attribute Pattern.VB_Description = "������������� ������ ����������� ��������� ��� �������."
    GuardEmptyString Value, Source:=TypeName(Me)
    
    This.Pattern = Value
End Property

'@Description "���������� True, ���� ����� ������������ ������� ����������� ���������; � ��������� ������ - False."
Private Function IDetector_IsMatch(ByVal Text As String) As Boolean
Attribute IDetector_IsMatch.VB_Description = "���������� True, ���� ����� ������������ ������� ����������� ���������; � ��������� ������ - False."
    GuardDefaultInstance Me, RegexDetector, Source:=TypeName(Me)
    
    IDetector_IsMatch = RegEx.IsMatch(Text, This.Pattern)
End Function

Public Function Value(ByVal GroupIndex As Integer, ByVal Text As String) As String
    Value = RegEx.GroupValue(Text, This.Pattern, GroupIndex)
End Function
