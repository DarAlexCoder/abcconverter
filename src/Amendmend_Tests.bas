Attribute VB_Name = "Amendmend_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

#Const LateBind = LateBindTests

#If LateBind Then
    Private Assert As Object
#Else
    Private Assert As Rubberduck.AssertClass
    #End If

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    #If LateBind Then
        Set Assert = CreateObject("Rubberduck.AssertClass")
    #Else
        Set Assert = New Rubberduck.AssertClass
    #End If
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub Substraction_Test()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAmendmend
Dim Original As IAmendmend


    'Act:
    Set Sut = Amendmend.Substraction
    Set Original = Amendmend

    'Assert:
    Assert.AreNotSame Sut, Original

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub ToString_Substraction_Test()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAmendmend

    'Act:
    Set Sut = Amendmend.Substraction

    'Assert:
    Assert.AreEqual "(��)", Sut.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub ToString_FullCoded_AmendmendSyntax()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAmendmend

    'Act:
    Set Sut = Amendmend.Create("�", 1, 1.15)

    'Assert:
    Assert.AreEqual "(�1.1,15)", Sut.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
