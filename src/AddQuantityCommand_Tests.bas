Attribute VB_Name = "AddQuantityCommand_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

Private Assert As Object

Private Reciever_ As IUnitRate

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    Set Assert = CreateObject("Rubberduck.AssertClass")
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestInitialize
Private Sub TestInitialize()
    'This method runs before every test in the module..
    Set Reciever_ = UnitRate.Construct("����_01-01-01", "test", "unit", "12,345")
End Sub

'@TestCleanup
Private Sub TestCleanup()
    'this method runs after every test in the module.
    Set Reciever_ = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_ZeroQuantity_DoesNothing()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As ICommand
    Set Sut = AddQuantityCommand.Create(0#)

    'Act:
    Sut.Execute Reciever_

    'Assert:
    Assert.AreEqual "�0101-01'12,345''+#test*", Reciever_.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_NullReciever_DoesNothing()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As ICommand
    Set Sut = AddQuantityCommand.Create(12.3)

    'Act:
    Sut.Execute Nothing

    'Assert:
    Assert.AreEqual "�0101-01'12,345''+#test*", Reciever_.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Execute_CorrectQuantity_ChangesUnitRateQuantity()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As ICommand
    Set Sut = AddQuantityCommand.Create(98.765)

    'Act:
    Sut.Execute Reciever_

    'Assert:
    Assert.AreEqual "�0101-01'12,345+98,765''+#test*", Reciever_.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
