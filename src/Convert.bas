Attribute VB_Name = "Convert"
'@Folder("Text")
Option Explicit

Public Function ToDouble(ByVal Text As String) As Double
    Dim InvariantSeparator As String
    InvariantSeparator = Replace(Text, ".", Application.DecimalSeparator)
    
    ToDouble = CDbl(InvariantSeparator)
End Function
