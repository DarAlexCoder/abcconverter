VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "AbcGuid"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Attribute VB_Ext_KEY = "Rubberduck" ,"Predeclared Class Module"
'@IgnoreModule WriteOnlyProperty
'@ModuleAttribute VB_Ext_KEY, "Rubberduck", "Predeclared Class Module"
'@Folder("Entities.Abc")
'@PredeclaredId
Option Explicit

Implements IAbcGuid

Private Code_ As String
Private StartChar_ As String

Public Property Get StartChar() As String
    StartChar = StartChar_
End Property

Private Property Get IAbcGuid_Code() As String
    IAbcGuid_Code = Code_
End Property
Public Property Let Code(ByVal Value As String)
    If Value Like "*�*" Then
        StartChar_ = "�"
    ElseIf Value Like "*�*" Then
        StartChar_ = "�"
    Else
        StartChar_ = "�"
    End If
    
    Code_ = RegEx.Match(Value, "(\d+\-?)+")
End Property

Private Function IAbcGuid_ToString() As String
    If 0 = Len(Code_) Then Exit Function
    
    IAbcGuid_ToString = StartChar & VBA.Replace(Code_, "-", vbNullString, 1, 1)
End Function

Public Function From(ByVal Code As String) As IAbcGuid
    Dim Result As AbcGuid
    Set Result = New AbcGuid
    
    With Result
        .Code = Code
    End With
    
    Set From = Result
End Function
