Attribute VB_Name = "AbcGuid_Tests"
Option Explicit
Option Private Module

'@TestModule
'@Folder("Tests")

#Const LateBind = LateBindTests

#If LateBind Then
    Private Assert As Object

#Else
    Private Assert As Rubberduck.AssertClass
    #End If

'@ModuleInitialize
Private Sub ModuleInitialize()
    'this method runs once per module.
    #If LateBind Then
        Set Assert = CreateObject("Rubberduck.AssertClass")
    #Else
        Set Assert = New Rubberduck.AssertClass
    #End If
End Sub

'@ModuleCleanup
Private Sub ModuleCleanup()
    'this method runs once per module.
    Set Assert = Nothing
End Sub

'@TestMethod("Uncategorized")
Private Sub From_CorrectString_AreNotSame()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAbcGuid

    'Act:
    Set Sut = AbcGuid.From("���� 10-01-10")

    'Assert:
    Assert.AreNotSame AbcGuid, Sut

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub Code_CorrectString_AreNotSame()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAbcGuid

    'Act:
    Set Sut = AbcGuid.From("���� 10-01-10")

    'Assert:
    Assert.AreEqual "10-01-10", Sut.Code

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub AsString_CorrectBuildString_Test()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAbcGuid

    'Act:
    Set Sut = AbcGuid.From("���� 10-01-10")

    'Assert:
    Assert.AreEqual "�1001-10", Sut.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub AsString_CorrectAssemblyString_Test()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAbcGuid

    'Act:
    Set Sut = AbcGuid.From("����� 10-01-10")

    'Assert:
    Assert.AreEqual "�1001-10", Sut.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub AsString_CorrectComissioningString_Test()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAbcGuid

    'Act:
    Set Sut = AbcGuid.From("����� 10-01-10")

    'Assert:
    Assert.AreEqual "�1001-10", Sut.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub

'@TestMethod("Uncategorized")
Private Sub AsString_EmptyString_ReturnsEmptyString()
    On Error GoTo TestFail
    
    'Arrange:
    Dim Sut As IAbcGuid

    'Act:
    Set Sut = AbcGuid.From(vbNullString)

    'Assert:
    Assert.AreEqual vbNullString, Sut.ToString

TestExit:
    Exit Sub
TestFail:
    Assert.Fail "Test raised an error: #" & Err.Number & " - " & Err.Description
    Resume TestExit
End Sub
