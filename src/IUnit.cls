VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "IUnit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = False
Attribute VB_Exposed = False
Attribute VB_Description = "������������ ������� ���������, ������� ����� ���� ������������ � ���� ��������� � ����� ������� ���������."
'@IgnoreModule IntegerDataType
'@Interface
'@Folder("Entities.Interfaces")
'@ModuleDescription "������������ ������� ���������, ������� ����� ���� ������������ � ���� ��������� � ����� ������� ���������."
Option Explicit

'@Description "����� ������� ��������� (��������, "��" � �������� ������ "10 ��")"
Public Property Get Text() As String
End Property

'@Description "����� ������� ��������� (��������, �� � �������� ������ 10 ��)"
Public Property Get Multiplier() As Long
Attribute Multiplier.VB_Description = "����� ������� ��������� (��������, �� � �������� ������ 10 ��)"
End Property

'@Description "���������� ������, ��� �������������� ���������� ������� � ������ ��������� ������� ��������� � ������ �� ���."
Public Property Get Convert() As IAbcElement
Attribute Convert.VB_Description = "���������� ������, ��� �������������� ���������� ������� � ������ ��������� ������� ��������� � ������ �� ���."
End Property
