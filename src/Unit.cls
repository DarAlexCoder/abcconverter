VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "Unit"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
'@Folder("VBAProject")
'@PredeclaredId
Option Explicit

Implements IUnit

Private Multiplier_ As Integer
Private Text_ As String

Public Function From(ByVal Text As String) As IUnit
    Dim TrimedText As String: TrimedText = Trim$(Text)
    
    Dim MultiplierText As String: MultiplierText = �������(TrimedText, "^\d*")
    Text_ = Trim$(Replace(TrimedText, MultiplierText, vbNullString))
    
    Dim Result As New Unit
    With Result
        If 0 < Len(MultiplierText) Then
            .Multiplier = CInt(MultiplierText)
        End If
        .Text = Trim$(Replace(TrimedText, MultiplierText, vbNullString))
    End With
    
    Set From = Result
End Function

Private Sub Class_Initialize()
    Multiplier_ = 1
End Sub

Private Property Get IUnit_Text() As String
    IUnit_Text = Text_
End Property
Public Property Let Text(ByVal Value As String)
    Text_ = Value
End Property

Private Property Get IUnit_Multiplier() As Integer
    IUnit_Multiplier = Multiplier_
End Property
Public Property Let Multiplier(ByVal Value As Integer)
    Multiplier_ = Value
End Property

Private Function IUnit_ToAbcText() As String
    IUnit_ToAbcText = IIf(Multiplier_ = 1, vbNullString, "." & Multiplier_)
End Function
